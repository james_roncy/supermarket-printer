var Printer = require('node-printer');
var options = {
    media: 'Custom.100x100mm',
    n: 3
};
 
// Get available printers list
const list = Printer.list();

var printer = new Printer(list[0]);
var text = 'Sample Printer'
printer.printText(text, options)